(ns re-frame-collapsible-panel.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [re-frame-collapsible-panel.core-test]))

(doo-tests 're-frame-collapsible-panel.core-test)
