(ns re-frame-collapsible-panel.events
  (:require
   [re-frame.core :as re-frame]
   [re-frame-collapsible-panel.db :as db]
   ))

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))
