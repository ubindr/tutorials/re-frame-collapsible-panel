(ns re-frame-collapsible-panel.example
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [re-frame-collapsible-panel.subs :as subs]))

(re-frame/reg-event-db
  :widget/toggle
  (fn [db [_ id]]
    (update-in db [:panels id] not)))

(re-frame/reg-sub
  :widget/state
  (fn [db [_ id]]
    (get-in db [:panels id])))

(defn example-compontent []
  (let [s (reagent/atom 0)]
    (js/setInterval #(swap! s inc) 1000)
    (fn []
      [:div @s])))

(defn panel [id title child]
  (let [s (reagent/atom {:open false})]
    (fn [_ title child]
      (let [open? @(re-frame/subscribe [:widget/state id])
            child-height (:child-height @s)]
        [:div
         [:div
          {:on-click #(re-frame/dispatch [:widget/toggle id])
           :style {:background-color "#ddd"
                   :padding "0 1em"}}
          [:div
           {:style {:float "right"}}
           (if open? "-" "+")]
          title]
         [:div {:style {:max-height   (if open?
                                        child-height
                                        0)
                        :transition "max-height 0.8s"
                        :overflow "hidden"}}
          [:div
           {:ref #(when %
                    (swap! s assoc :child-height (.-clientHeight %)))
            :style {:background-color "#eee"
                    :padding "0 1em"}}
           child]]]))))


(defn example-panel []
  [:div
   [panel :ex-1 "Example Component"
    [example-compontent]]
   [:div
    [:p]
    [:a {:href "#/about"}
     "go to About Page"]
    [:p]
    [:a {:href "#/"}
     "go to Home Page"]]])
